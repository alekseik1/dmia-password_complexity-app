# ONLY export requirements.txt from poetry
FROM python:3.9.5 AS builder

# Install and setup poetry
RUN pip install -U pip \
    && apt-get update \
    && apt-get install -y curl netcat \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
ENV PATH="${PATH}:/root/.poetry/bin"
COPY pyproject.toml .
COPY poetry.lock .
RUN poetry export -f requirements.txt --output /requirements.txt

# Real application
FROM python:3.9.5
# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE 1
# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED 1

COPY --from=builder /requirements.txt .
RUN pip install --no-cache-dir -r /requirements.txt
RUN useradd -m webuser
USER webuser
WORKDIR /app

COPY . .

ENV PYTHONPATH /app/:$PYTHONPATH
ENTRYPOINT ["./entrypoint.sh"]