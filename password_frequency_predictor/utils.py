import functools
from contextlib import contextmanager

from loguru import logger


# https://loguru.readthedocs.io/en/stable/resources/recipes.html#logging-entry-and-exit-of-functions-with-a-decorator
def logger_wraps(
    *,
    entry: bool = True,
    exit: bool = True,
    record_variables: bool = False,
    level: str = "DEBUG",
):
    def wrapper(func):
        name = func.__name__

        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            logger_ = logger.opt(depth=1)
            if entry:
                if record_variables:
                    logger_.log(
                        level, "Entering '{}' (args={}, kwargs={})", name, args, kwargs
                    )
                else:
                    logger_.log(level, "Entering '{}'", name)
            result = func(*args, **kwargs)
            if exit:
                if record_variables:
                    logger_.log(level, "Exiting '{}' (result={})", name, result)
                else:
                    logger_.log(level, "Exiting '{}'", name)
            return result

        return wrapped

    return wrapper


@contextmanager
def log_enter_exit(message: str, level: str = "DEBUG"):
    logger_ = logger.opt(depth=2)
    logger_.log(level, message)
    yield
    logger_.log(level, f"(DONE) {message}")


def static_init(cls):
    if getattr(cls, "static_init", None):
        cls.static_init()
    return cls
