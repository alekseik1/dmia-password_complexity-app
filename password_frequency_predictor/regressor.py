import os
import re
from pathlib import Path
from typing import Iterable, Union

import numpy as np
import pandas as pd
from catboost import CatBoostRegressor
from loguru import logger
from nltk.corpus import words  # Корпус слов для проверки того, что пароль содержит слово
from password_strength import PasswordStats
from utils import log_enter_exit, logger_wraps, static_init


@static_init
class PasswordComplexityRegressor:
    """Управление моделью.

    Произвести прогноз, переобучить.
    """

    ENGLISH_WORDS = None

    @classmethod
    @logger_wraps()
    def static_init(cls) -> None:
        """Load words corpus from NLTK library (download them from Internet if
        necessary)

        :return:
        """
        if cls.ENGLISH_WORDS is None:
            logger.info("Trying to load words from NLTK library")
            try:
                cls.ENGLISH_WORDS = set(words.words())
            except Exception:
                logger.info("Did not find local corpus. Downloading from Internet")
                import nltk

                nltk.download("words")
                cls.ENGLISH_WORDS = set(words.words())

    @logger_wraps(record_variables=True)
    def __init__(
        self,
        weights_file: Union[str, Path] = "",
        iterations: int = 300,
        depth: int = 11,
        l2_leaf_reg: int = 20,
        learning_rate: float = 0.5,
        loss_function: str = "RMSE",
        logging_level: str = "Silent",
    ) -> None:
        """Конструктор. Если есть файл модели, то читает модель.

        Если нет файла модели - нужно обучить модель.
        :param weights_file: Путь к файлу модели *.cbm
        """
        self.path_to_model = weights_file

        # https://catboost.ai/docs/concepts/python-reference_catboostregressor.html
        self.model = CatBoostRegressor(
            iterations=iterations,
            depth=depth,
            l2_leaf_reg=l2_leaf_reg,
            learning_rate=learning_rate,
            loss_function=loss_function,
            logging_level=logging_level,
        )
        if os.path.isfile(weights_file):
            with log_enter_exit(f"loading model from {weights_file}"):
                # https://catboost.ai/docs/concepts/python-reference_catboostregressor_load_model.html
                self.model.load_model(self.path_to_model)
        else:
            logger.info("No weights provided. Don't forget to fit model before usage!")

    def predict(self, passwords: Union[Iterable, str, pd.DataFrame]) -> np.ndarray:
        """Производит прогноз. Сколько раз встречается пароль в 1_000_000
        реальных паролей.

        :param passwords: Один пароль в строке, список паролей или test dataframe.
        :return: Одно значение прогноза или массив значений.
        """
        if isinstance(passwords, list):
            features = [
                PasswordComplexityRegressor.__generate_features(password)
                for password in passwords
            ]
        elif isinstance(passwords, str):
            features = PasswordComplexityRegressor.__generate_features(passwords)
        elif isinstance(passwords, pd.DataFrame):
            features = PasswordComplexityRegressor.__generate_dataframe_with_features(
                passwords
            )
        else:
            raise TypeError("Invalid type of 'passwords' argument.")

        # https://catboost.ai/docs/concepts/python-reference_catboostregressor_predict.html
        with log_enter_exit("predicting"):
            y_pred = self.model.predict(features)
        return np.exp(y_pred) - 1

    @logger_wraps(record_variables=False)
    def fit(self, df: pd.DataFrame, save_to: Union[str, Path] = False) -> None:
        y_train = df["Times"].apply(lambda x: np.log(x + 1))
        x_train = self.__generate_dataframe_with_features(df)

        # https://catboost.ai/docs/concepts/python-reference_catboostregressor_fit.html
        with log_enter_exit("training model"):
            self.model.fit(x_train, y_train)

        # https://catboost.ai/docs/concepts/python-reference_catboostregressor_save_model.html
        if save_to:
            try:
                with log_enter_exit(f"saving model weights to {self.path_to_model}"):
                    self.model.save_model(self.path_to_model, format="cbm")
            except Exception:
                logger.error(f"could not save weights to {save_to}")

    @staticmethod
    def __generate_features(password: str) -> list:
        """Функция генерит фичи для одного пароля."""
        df = pd.DataFrame([password], columns=["Password"])
        PasswordComplexityRegressor.__generate_dataframe_with_features(df)
        return list(df.loc[0])

    @staticmethod
    @logger_wraps(record_variables=False)
    def __generate_dataframe_with_features(df: pd.DataFrame) -> pd.DataFrame:
        """Функция генерит фичи для всех паролей в датасете.

        Принимает в качестве параметра pandas.DataFrame. Работает
        inplace.
        """
        with log_enter_exit("calculating zeros"):
            df["0_count"] = df["Password"].apply(lambda x: str(x).count("0"))
        with log_enter_exit("calculating ones"):
            df["1_count"] = df["Password"].apply(lambda x: str(x).count("1"))
        with log_enter_exit("calculating twos"):
            df["2_count"] = df["Password"].apply(lambda x: str(x).count("2"))

        with log_enter_exit("calculating password_strength scores"):
            df["repeated_patterns_length"] = df["Password"].apply(
                lambda x: PasswordStats(str(x)).repeated_patterns_length
            )
            df["weakness_factor"] = df["Password"].apply(
                lambda x: PasswordStats(str(x)).weakness_factor
            )
            df["entropy_bits"] = df["Password"].apply(
                lambda x: PasswordStats(str(x)).entropy_bits
            )

        with log_enter_exit("calculating each string length"):
            df["char_count"] = df["Password"].apply(lambda x: len(str(x)))

        with log_enter_exit("calculating number of digits password"):
            digit_count = df["Password"].apply(
                lambda x: len([x for x in str(x) if x.isdigit()])
            )
            df["digit_ratio"] = digit_count / df["char_count"]

        with log_enter_exit("calculating number of symbolic passwords"):
            isalpha_count = df["Password"].apply(
                lambda x: len([x for x in str(x) if x.isalpha()])
            )
            df["alpha_ratio"] = isalpha_count / df["char_count"]

        with log_enter_exit("calculating number of vowels"):
            vowels = ["a", "e", "i", "o", "u"]
            vowels_count = df["Password"].apply(
                lambda x: len([x for x in str(x).lower() if x in vowels])
            )
            df["vowels_ratio"] = vowels_count / df["char_count"]  # Доля гласных

        upper_case = [
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z",
        ]
        with log_enter_exit("calculating number of uppercase letters"):
            upper_case_count = df["Password"].apply(
                lambda x: (len([x for x in str(x) if x in upper_case]))
            )
            df["upper_case_ratio"] = (
                upper_case_count / df["char_count"]
            )  # Доля заглавных букв

        # Проверим, не содержит ли пароль реального слова
        with log_enter_exit("calculating how much passwords contain real words"):
            df["is_real_word"] = df["Password"].apply(
                lambda x: 1
                if re.sub(r"[0-9]+", "", str(x).lower())
                in PasswordComplexityRegressor.ENGLISH_WORDS
                else 0
            )

        with log_enter_exit("calculating unique chars ratios"):
            unique_chars_count = df["Password"].apply(lambda x: len(set(str(x))))
            df["unique_chars_ratio"] = (
                unique_chars_count / df["char_count"]
            )  # Доля уникальных символов

        with log_enter_exit("removing target columns"):
            if "Password" in df.columns:
                del df["Password"]
            if "Times" in df.columns:
                del df["Times"]

        return df


if __name__ == "__main__":
    basedir = Path(__file__).absolute().parent
    input_data_dir = basedir.parent / "input_data"
    TRAIN_DATA = input_data_dir / "train.csv"
    WEIGHTS_DATA = input_data_dir / "weights.cbm"

    model = PasswordComplexityRegressor(weights_file=WEIGHTS_DATA)
    # df_train = pd.read_csv(TRAIN_DATA)
    # model.fit(df_train, save_to=WEIGHTS_DATA)

    df_passwords = pd.read_csv(input_data_dir / "Xtest.csv")
    predictions = model.predict(df_passwords)
    df_passwords["Times"] = predictions
    results = df_passwords[["Id", "Times"]]
    results.to_csv(input_data_dir / "preds.csv", index=False)
