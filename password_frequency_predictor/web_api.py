from pathlib import Path

from fastapi import FastAPI, Form, Request
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from loguru import logger

from password_frequency_predictor.regressor import PasswordComplexityRegressor


basedir = Path(__file__).absolute().parent
app = FastAPI()
app.logger = logger
# Add /static to website
app.mount(
    "/static",
    StaticFiles(directory=basedir.parent / "static"),
    name="static",
)
# NOTE: adjust to your path
model = PasswordComplexityRegressor(
    weights_file=basedir.parent / "input_data" / "weights.cbm"
)

templates = Jinja2Templates(directory=str(basedir.parent / "static" / "templates"))


@app.get("/", response_class=HTMLResponse)
async def render_page(request: Request):
    return templates.TemplateResponse(
        "index.html", {"request": request, "password": "", "prediction": 0}
    )


@app.post("/")
async def reload_predictions_page(request: Request, password: str = Form("")):
    prediction = model.predict(password)
    return templates.TemplateResponse(
        "index.html",
        {"request": request, "password": password, "prediction": prediction},
    )


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=7777)
