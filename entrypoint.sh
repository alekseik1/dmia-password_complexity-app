#!/bin/bash
HOST=${HOST:-"0.0.0.0"}
PORT=${PORT:-7777}
cd password_frequency_predictor || exit 0
python -m uvicorn web_api:app --host "$HOST" --port "$PORT"